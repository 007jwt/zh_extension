//代码与中文对应关系
const codeObj = {
    0:'请求成功',
    1:'帐号或密码错误',
    2:'服务器异常',
    3:'请求错误',
    4:'该帐号已存在',
    5:'邮件已发送,请在15分钟内认证',
    6:'帐号注册成功',
    7:'注册人数已超上限，请联系作者索取帐号',
    8:'帐号非VIP，不能使用该功能',
    9:'帐号不存在',
    10:'邮件已发送,15分钟内请勿重复操作',
    11:'验证码错误',
    12:'密码修改成功',
    400:'用户认证超时',
}

let getRes = function(code,data,parseArr) {//pareArr需要转换成JSON的字段，可以解析多个
    if(parseArr){
        if(data instanceof Array){//数组处理方式
            for (let i = 0; i < data.length; i++) {
                let obj = data[i];
                for (let j = 0; j < parseArr.length; j++) {
                    let filed = parseArr[j];
                    if(obj[filed])
                        obj[filed] = JSON.parse(obj[filed]);
                }
            }
        }else{//对象的处理方式
            for (let j = 0; j < parseArr.length; j++) {
                let filed = parseArr[j];
                if(data[filed])
                    data[filed] = JSON.parse(data[filed]);
            }
        }
    }
    return {
        code,
        msg:codeObj[code],
        data
    }
}

module.exports = { getRes }