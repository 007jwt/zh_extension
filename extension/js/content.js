console.log("welcome to 07's zhihu extension");

start_mutation();

function start_mutation(){
    const targetNode = document.getElementById('root');

    // Options for the observer (which mutations to observe)
    const config = { attributes: false, childList: true, subtree: true };

    // Callback function to execute when mutations are observed
    const callback = function(mutationsList, observer) {
        // Use traditional 'for loops' for IE 11
        for(let mutation of mutationsList) {
            // console.log(mutation);
            if (mutation.type === 'childList' && $(mutation.target).hasClass("RichContent")
            && $(mutation.target).find(".q_check_class").length==0) {
                if($(mutation.target).find(".VoteButton:disabled").length==0){
                    continue;
                }
                
                // console.log($(mutation.target).find(".q_check_class"));
                // console.log(mutation);
                add_check(mutation);
            }
        }
    };


    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);
}
// Later, you can stop observing
// observer.disconnect();


function add_check(mutation){
    //通过MutationObserver在页面加载或变化时添另查重按钮
    if($(mutation.target).parent().hasClass("AnswerItem")){
        if($(mutation.target).find(".AnswerItem-editButton").length==0){
            return;
        }
    }

    var action_div = $(mutation.target).find(".ContentItem-rightButton:last");
    
    //查重按钮
    var tmp123 = $('<button type=\"button\" style=\"color:red\" class=\"q_check_class Button ContentItem-action Button--plain Button--withIcon Button--withLabel\">查重</button>');
    tmp123.click(function(){
        var content_div = $(this).parent().parent().parent();

        // var c_href = par.find(".ContentItem-title:first").find("a:first").attr("href");

        if(!content_div.hasClass("RichContent")){
            content_div = $(this).parent().parent();
        }

        content_div = content_div.children(".RichContent-inner:first");

        // console.log(content_div[0].innerText);

        var c_data={
            mode:"copy_cp",
            text:content_div[0].innerText,
        };
        // console.log(c_data);
        sendData(c_data);
        $(this).text("已复制");
    })
    action_div.before(tmp123);

}


function rgb2hex(rgb) {
	rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	function hex(x) {
		return ("0" + parseInt(x).toString(16)).slice(-2);
	}
	return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

async function sendData(data){
    var res = await sendmsg(data);
    // console.log(res);
	return res;
}

function sendmsg(data) {
    return new Promise((resolve, reject) => {
        if (data != null) {
            chrome.extension.sendMessage(data, function(response) {
                // console.log(response);
                resolve(response);
            });
        } else {
            reject(null);
        }
    });
}