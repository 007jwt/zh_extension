function formatDate(date, fmt) {//日期格式化
    // var fmt = 'yyyy-MM-dd hh:mm:ss'
    var date = new Date(date);
    var o = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

function pay_info() {
    let data = [
        {
            type: "month",
            price: 1900,
            month: 1,
            text: `<button type="button" class="btn btn-primary" ><span>19元/月，</span><span style="text-decoration:line-through">原价29元/月</span></button>`,
        },
        {
            type: "year",
            price: 19900,
            month: 12,
            text: `<button type="button" class="btn btn-primary" ><span>199元/年，</span><span style="text-decoration:line-through">原价299元/年<span></button>`,
        }
    ]
    return data;
}

function get_pay_one(type) {
    let list = pay_info();
    for (var i = 0; i < list.length; i++) {
        if (list[i].type == type) {
            return list[i];
        }
    }
    return null;
}

function pay_prex() {
    return "IP:端口";
}

function randomWord(size) {//size是生成随机密码的位数

    var seed = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'p', 'Q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1',
        '2', '3', '4', '5', '6', '7', '8', '9'
    );//数组
    var seedlength = seed.length;//数组长度
    var createPassword = '';
    for (let i = 0; i < size; i++) {
        var a = Math.floor(Math.random() * seedlength);
        createPassword += seed[a];
    }
    return createPassword;
}

module.exports = { formatDate, pay_info, get_pay_one, pay_prex ,randomWord}