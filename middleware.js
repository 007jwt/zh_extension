const cors = require('koa2-cors');
const bodyParser = require('koa-bodyparser')
const path = require('path')
const static = require('koa-static')
const convert = require('koa-convert');

module.exports = (app) => {
    app.use(cors());
    app.use(convert(bodyParser({
        enableTypes:['json', 'form', 'text'],
        formLimit:"100mb",
        queryString:{
            parameterLimit:100000000000000
        }
    })));
    app.use(bodyParser());
    app.use(static(path.join(__dirname, './public')))
}