const Router = require('koa-router')
const checkToken = require('./middleware/token.js')
const router = new Router({
    prefix: '/api'//单纯做后台api
});

const login = require('./routes/login.js')
const user = require('./routes/user.js')
const goods = require('./routes/goods.js')
const content = require('./routes/content.js')
const pay = require('./routes/pay.js')
const history = require('./routes/history.js')
const checkrepeat = require('./routes/checkrepeat.js')
const checklog = require('./routes/checklog.js')
// const out_excel = require('./routes/out_excel.js')


module.exports = (app) => {
    router.use('/login', login.routes())
    // router.use('/out_excel', out_excel.routes())
    router.use(checkToken);//验证token，以下路由要进行token验证，有些不需要验证的接口贪方便，放在了login,比如支付回调
    router.use('/user', user.routes())
    router.use('/goods', goods.routes())
    router.use('/content', content.routes())
    router.use('/pay', pay.routes())
    router.use('/history', history.routes())
    router.use('/checkrepeat', checkrepeat.routes())
    router.use('/checklog', checklog.routes())
    app.use(router.routes()).use(router.allowedMethods())
}