//评分历史

const Router = require('koa-router')
const { query,constInsert,exeTrans,constUpdate,paging,constructWhere } = require('../db')
const { getRes } = require('../res')
var fs = require("fs");
let history = new Router()



history.post('/getlist', async(ctx) => {
    try {
        let sql = "select * from userinfo where id=? and lasttime>now() and status>=0";
        let user = await query(sql,ctx.user.id);
        if(user.length==0){
            return ctx.response.body = getRes(8);
        }

        let prefix =  `select * from history`
        let pageSize = ctx.request.body.pageSize;
        let pageNo = ctx.request.body.pageNo;
        let orderby = ctx.request.body.orderby;
        let ordertype = ctx.request.body.ordertype;
        let queryObj = {
            user_id:ctx.user.id,
        };
        let whereObj = constructWhere(queryObj);
        let total = await query(`select count(1) as cnt from history ${whereObj.where}`,whereObj.values);
        let res = await paging(prefix,queryObj,pageSize,pageNo,`${orderby} ${ordertype}`);
        let obj = getRes(0,res);
        obj.pageNo = pageNo ? pageNo : 1
        obj.pageSize = pageSize ? pageSize : 10;
        obj.total = total[0].cnt;
        ctx.response.body = obj;
    }catch (e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

history.post('/remove', async(ctx) => {
    let qry = JSON.parse( JSON.stringify( ctx.request.body));
    delete qry.token;
    qry.user_id=ctx.user.id;
    try {
        console.log(qry);
        
        let sql = "delete from history where user_id=? and id=?";
        let res = await query(sql,[qry.user_id,qry.id]);

        ctx.response.body = getRes(0,res);
    }catch (e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

history.post('/save', async(ctx) => {
    let qry = JSON.parse( JSON.stringify( ctx.request.body));
    delete qry.token;
    qry.user_id=ctx.user.id;
    try {
        console.log(qry);

        let tranFunc = async (conn) => {//执行事务
            try{

                let sql = "select * from history where user_id=? and keyword=?";
                let res = await query(sql,[qry.user_id,qry.key]);

                if(res.length>0){
                    let del_sql = "delete from history where user_id=? and keyword=?";
                    await query(del_sql,[qry.user_id,qry.key]);
                }
                
                let his = {
                    user_id:qry.user_id,
                    keyword:qry.key,
                    json:JSON.stringify(qry.obj),
                    creattime:new Date()
                }

                await constInsert('history',his);

                return Promise.resolve('success');
            }catch (e){
                return Promise.reject(e);
            }
        }
        await exeTrans(tranFunc);
        ctx.response.body = getRes(0);
    }catch (e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

module.exports = history;