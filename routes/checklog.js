//问题监控，弃用

const Router = require('koa-router')
const { query,constInsert,exeTrans,constUpdate,paging,constructWhere } = require('../db')
const { getRes } = require('../res')
var fs = require("fs");
let checklog = new Router()


checklog.post('/getone', async(ctx) => {
    try {
        let sql = "select * from checklogques order by num,id limit 0,1";
        let obj = await query(sql);
        
        ctx.response.body = getRes(0,obj);
    }catch (e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

checklog.post('/save', async(ctx) => {
    let qry = JSON.parse( JSON.stringify( ctx.request.body));
    delete qry.token;
    try {
        console.log(qry);
        qry = qry.data;
        let tranFunc = async (conn) => {//执行事务
            try{
                
                let his = {
                    visit:qry.visit,
                    anser:qry.answer,
                    follow:qry.follow,
                    check_q_id:qry.id,
                    creattime:new Date()
                }

                await constInsert('checklog',his);

                await query(`update checklogques set num = num+1 where qid=? `,qry.id);

                return Promise.resolve('success');
            }catch (e){
                return Promise.reject(e);
            }
        }
        await exeTrans(tranFunc);
        ctx.response.body = getRes(0);
    }catch (e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

module.exports = checklog;