//文章查重接口

const Router = require('koa-router')
const { query, constInsert, exeTrans, constUpdate, paging, constructWhere } = require('../db')
const { getRes } = require('../res')
let request = require('request')
let urlencode = require('urlencode')
let checkrepeat = new Router()

function check(url, qry) {
    //调用5118百度接口
    return new Promise(function (resolve, reject) {
        request({
            url: url,
            method: "POST",
            json: true,
            headers: {
                "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "Authorization": "接口认证参数"
            },
            body: "txt=" + qry
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                resolve(body); // 请求成功的处理逻辑
            } else {
                reject();
            }
        });
    });
}

checkrepeat.post('/check', async (ctx) => {
    let qry = JSON.parse(JSON.stringify(ctx.request.body));
    // console.log(qry);
    delete qry.token;
    try {

        let sql = "select * from userinfo where id=? and lasttime>now() and status>=0";
        let user = await query(sql,ctx.user.id);
        if(user.length==0){
            return ctx.response.body = getRes(8);
        }
        
        let text = qry.text;
        // console.log(text);

        let res = await check("http://apis.5118.com/wyc/original", urlencode(text));
        // let res={errcode:0};
        console.log(res);
        if (res.errcode != 0) {
            return ctx.response.body = getRes(3);
        }

        let his = {
            user_id:ctx.user.id,
            creattime:new Date()
        }

        await constInsert('cp_history',his);

        let data = res.data;

        if (data==null || data.length == 0) {
            return ctx.response.body = getRes(0, []);
        }

        console.log(data);

        let list = [];
        for (var i = 0; i < data.length; i++) {
            let tmpl = data[i];
            if (tmpl.length == 0) {
                continue;
            }
            for (var k = 0; k < tmpl.length; k++) {

                var tmp = tmpl[k];

                if (tmp.OriginalValue == "serious" || tmp.OriginalValue == "secondary") {
                    if (k > 0) {
                        var tmplast = tmpl[k - 1];
                        console.log(tmplast.Content.length + tmplast.Sort);
                        console.log(tmp.Sort);
                        //文字相连
                        if (tmplast.Content.length + tmplast.Sort == tmp.Sort) {
                            if (tmplast.OriginalValue == tmp.OriginalValue) {
                                list[list.length - 1].Content = list[list.length - 1].Content + tmp.Content;
                            } else {
                                list.push(tmp);
                            }
                        } else {
                            list.push(tmp);
                        }
                    } else {
                        list.push(tmp);
                    }
                }
            }
        }
        
        ctx.response.body = getRes(0, list);
    } catch (e) {
        console.log(e);
        ctx.response.body = getRes(2, e);
    }
})

module.exports = checkrepeat;