const Router = require('koa-router')
const { query,constInsert,constUpdate,paging,constructWhere } = require('../db')
const { getRes } = require('../res')
const { formatDate } = require('../util')

let userinfo = new Router()


userinfo.get("/register_toke", async(ctx) => {//登录接口
    console.log('register.....................');
    try{
        let user = ctx.register;
        let qry = {
            mail:user.mail,
            pwd:user.pwd,
            name:user.mail,
            status:0
        }
        let total = await query(`select count(1) as cnt from userinfo`);
        if(total[0].cnt>=10000){
            ctx.response.body = '<html><body><div>注册人数已超上限，请联系作者索取帐号</div></body></html>';
        }else{
            let u_tal = await query(`select count(1) as cnt from userinfo where mail=?`,user.mail);
            if(u_tal[0].cnt>0){
                return ctx.response.body = '<html><body><div>该帐号已存在</div></body></html>';
            }
            
            let date = new Date();
            date.setMinutes(date.getMinutes()+60*24*3);
            qry.lasttime = formatDate(date,"yyyy-MM-dd hh:mm:ss"); 
            let res = await constInsert('userinfo',qry);
            ctx.response.body = '<html><body><div>请帐号注册成功</div></body></html>';
        }
    }catch(e){
        console.log(e);
        ctx.response.body = '<html><body><div>请求错误</div></body></html>';
    }
})

userinfo.post('/checktoken', async(ctx) => {//登录接口
    console.log('check token.....................');
    console.log(ctx.request.body);
    try{
        let user = ctx.user;

        user = {
            mail:user.mail,
            id:user.id
        }

        let usql = "select u.mail,u.lasttime from userinfo u where u.id=? and u.lasttime>now() and u.status>=0";
        let tmp = await query(usql,user.id);

        console.log(tmp);
        if(tmp.length>0){
            user.is_vip = 1;
            user.lasttime = tmp[0].lasttime;
        }

        ctx.response.body = getRes(0,user)
    }catch(e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

// userinfo.post('/getone', async(ctx) => {
//     let id = ctx.request.body.id;
//     let qry = `select u.*, d.name as deptName from userinfo u 
//                left join dept d on u.deptId = d.id where u.id=?`
//     console.log('&&&&&&&&&&&&query&&&&&&&&&&&&&&&')
//     console.log(qry)
//     try {
//         let res = await query(qry,id);
//         let resp = null
//         if(res.length > 0) resp = res[0];
//         ctx.response.body = getRes(0,resp);
//     }catch (e){
//         console.log(e);
//         ctx.response.body = getRes(2,e);
//     }
// })

// userinfo.post('/getall', async(ctx) => {
//     let prefix = `select u.*, d.name as deptName from userinfo u left join dept d on u.deptId = d.id`
//     let {name,gh,deptId} = ctx.request.body;
//     let queryObj = {'amb_u.name':name,'amb_u.gh':gh,deptId};
//     let whereObj = constructWhere(queryObj)
//     try {
//         let res = await query(`${prefix} ${whereObj.where}`,whereObj.values);
//         ctx.response.body = getRes(0,res);
//     }catch (e){
//         console.log(e);
//         ctx.response.body = getRes(2,e);
//     }
// })

// userinfo.post('/getlist', async(ctx) => {
//     let pageSize = ctx.request.body.pageSize;
//     let pageNo = ctx.request.body.pageNo;
//     let prefix =  `select u.*, d.name as deptName from userinfo u left join dept d on u.deptId = d.id`
//     let {name,gh,deptId} = ctx.request.body;
//     let queryObj = {'amb_u.name':name,'amb_u.gh':gh,deptId};
//     let whereObj = constructWhere(queryObj,true)
//     try {
//         let total = await query(`select count(1) as cnt from userinfo u ${whereObj.where}`,whereObj.values);
//         let res = await paging(prefix,queryObj,pageSize,pageNo,null);
//         let obj = getRes(0,res);
//         obj.pageNo = pageNo ? pageNo : 1
//         obj.pageSize = pageSize ? pageSize : 10;
//         obj.total = total[0].cnt;
//         ctx.response.body = obj;
//     }catch (e){
//         console.log(e);
//         ctx.response.body = getRes(2,e);
//     }
// })

userinfo.post('/save', async(ctx) => {
    let qry = JSON.parse( JSON.stringify( ctx.request.body));
    delete qry.token;
    try {
        let res = await constInsert('userinfo',qry);
        ctx.response.body = getRes(0,res);
    }catch (e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

userinfo.post('/update', async(ctx) => {
    console.log('***********************************8')
    console.log(ctx.request.body)
    let qry = JSON.parse( JSON.stringify( ctx.request.body));
    delete qry.token;
    console.log('&&&&&&&&&&&&query&&&&&&&&&&&&&&&')
    console.log(qry)
    try {
        let res = await constUpdate('userinfo',qry);
        ctx.response.body = getRes(0,res);
    }catch (e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

userinfo.post('/remove', async(ctx) => {
    try {
        let res = await query(`delete from userinfo where id = ?`,ctx.request.body.id);
        ctx.response.body = getRes(0,res);
    }catch (e){
        console.log(e);
        ctx.response.body = getRes(2,e);
    }
})

module.exports = userinfo;