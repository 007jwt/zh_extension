const koa = require('koa');
const middleware = require('./middleware.js');
const router = require('./router.js');
const app = new koa();

middleware(app);
router(app);
app.listen(5002);
console.log('[demo] start-quick is starting at port 5002')